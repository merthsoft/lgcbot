﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Merthsoft.WolframAlphaApi;
using System.Text;
using System.Threading;
using Merthsoft.WundergroundApi;
using Discord;
using LGCBot.DataModels;
using MySql.Data.MySqlClient;
using LinqToDB;
using RestSharp;

namespace LGCBot {
	partial class Bot {
        public void Cat(MessageEventArgs eventArgs, string command, IList<string> parameters) {
            string url = string.Format("api/images/get.php/format=src&api_key={0}", catApiKey);
            var client = new RestClient("http://thecatapi.com/");
            var response = client.Get(new RestRequest(url));
            if (response.StatusCode == System.Net.HttpStatusCode.OK) {
                using (var ms = new MemoryStream(response.RawBytes)) {
                    eventArgs.Channel.SendFile(response.ResponseUri.LocalPath, ms);
                }
            }
        }

        public void ListRoles(MessageEventArgs eventArgs, string command, IList<string> parameters) {
            StringBuilder sb = new StringBuilder();
            foreach (var server in client.Servers) {
                sb.AppendLine(server.Name + ":");
                foreach (var role in server.Roles) {
                    sb.AppendLine(string.Format("\t{0}: {1}", role.Name, role.Id));
                }
            }

            SendMessage(eventArgs.Channel, sb.ToString());
        }

        public void AddQuestion(MessageEventArgs eventArgs, string command, IList<string> parameters) {
            using (var db = new lgcbotDB("def")) {
                db.BeginTransaction();
                var question = new ExtraText() {
                    Text = string.Join(" ", parameters)
                };
                question.Id = Convert.ToInt32(db.InsertWithIdentity(question));

                var tag = db.Tags.FirstOrDefault(t => t.Tag_Column == "Question");

                if (tag == null) {
                    SendMessage(eventArgs.Channel, "{0}: Unable to add question.", eventArgs.User.NicknameMention);
                    OnLogMessage(this,
                        new LogMessageEventArgs(LogSeverity.Error, "AddQuestion", "Question tag not found...", null));
                }

                var questionTag = new ExtraTextTag() {
                    ExtraTextId = question.Id,
                    TagId = tag.Id
                };
                db.Insert(questionTag);
                db.CommitTransaction();

                SendMessage(eventArgs.Channel, "{0}: Question #{1} added.", eventArgs.User.NicknameMention, question.Id);
            }
        }

        public void Weather(MessageEventArgs eventArgs, string command, IList<string> parameters) {
			if (parameters.Count == 0) {
				SendMessage(eventArgs.Channel, "{0}: Location not included.", eventArgs.User.NicknameMention);
				return;
			}

			string ret = null;
			Task t = Task.Factory.StartNew(() => {
				try {
					ret = Wunderground.GetConditions(string.Join(" ", parameters));
				} catch {
					ret = "Could not get the weather.";
				}
				SendMessage(eventArgs.Channel, "{0}: {1}", eventArgs.User.NicknameMention, ret);
			}
			);
		}

        public void Calc(MessageEventArgs eventArgs, string command, IList<string> parameters) {
            string math = string.Join(" ", parameters);
            logInfo("Calc", "Calculating: {0}", math);
			string ret = null;
			Task t = Task.Factory.StartNew(() => {
                try {
                    ret = WolframAlpha.Calculate(math);
                } catch {
                    ret = "Could not calculate.";
                }
                SendMessage(eventArgs.Channel, "{0}: {1}", eventArgs.User.NicknameMention, ret);
            }
            );
		}
	}
}
