-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: localhost    Database: lgcbot
-- ------------------------------------------------------
-- Server version	5.6.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Command`
--

DROP TABLE IF EXISTS `Command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Command` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` text CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CommandText`
--

DROP TABLE IF EXISTS `CommandText`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CommandText` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CommandId` int(11) NOT NULL,
  `Text` text CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_CommandText_Command` (`CommandId`),
  CONSTRAINT `FK_CommandText_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ExtraText`
--

DROP TABLE IF EXISTS `ExtraText`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExtraText` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Text` text CHARACTER SET utf8mb4 NOT NULL,
  `Used` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=1088 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ExtraTextTag`
--

DROP TABLE IF EXISTS `ExtraTextTag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExtraTextTag` (
  `ExtraTextId` int(11) NOT NULL,
  `TagId` int(11) NOT NULL,
  PRIMARY KEY (`ExtraTextId`,`TagId`),
  KEY `FK_EXTRATEXTTAG_TAG` (`TagId`),
  CONSTRAINT `FK_ExtraTextTag_ExtraText` FOREIGN KEY (`ExtraTextId`) REFERENCES `ExtraText` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ExtraTextTag_Tag` FOREIGN KEY (`TagId`) REFERENCES `Tag` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Function`
--

DROP TABLE IF EXISTS `Function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Function` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Image`
--

DROP TABLE IF EXISTS `Image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Image` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Image` longblob NOT NULL,
  `Extension` text CHARACTER SET utf8mb4 NOT NULL,
  `Used` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ImageTag`
--

DROP TABLE IF EXISTS `ImageTag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ImageTag` (
  `ImageId` int(11) NOT NULL,
  `TagId` int(11) NOT NULL,
  PRIMARY KEY (`ImageId`,`TagId`),
  KEY `FK_IMAGETAG_IMAGE` (`ImageId`),
  KEY `FK_IMAGETAG_TAG` (`TagId`),
  CONSTRAINT `FK_ImageTag_Image` FOREIGN KEY (`ImageId`) REFERENCES `Image` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ImageTag_Tag` FOREIGN KEY (`TagId`) REFERENCES `Tag` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Response`
--

DROP TABLE IF EXISTS `Response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Response` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CommandId` int(11) NOT NULL,
  `NumParameters` int(11) NOT NULL DEFAULT '0',
  `Text` text CHARACTER SET utf8mb4,
  `ImageTagId` int(11) DEFAULT NULL,
  `FunctionId` int(11) DEFAULT NULL,
  `ExtraTextTagId` int(11) DEFAULT NULL,
  `FailedChannelCheckText` text CHARACTER SET utf8mb4,
  `FailedRoleCheckText` text CHARACTER SET utf8mb4,
  `FailedUserCheckText` text CHARACTER SET utf8mb4,
  `FailedParameterCheckText` text CHARACTER SET utf8mb4,
  PRIMARY KEY (`Id`),
  KEY `FK_RESPONSE_COMMAND` (`CommandId`),
  KEY `FK_RESPONSE_EXTRATEXTTAG` (`ExtraTextTagId`),
  KEY `FK_RESPONSE_FUNCTION` (`FunctionId`),
  KEY `FK_RESPONSE_IMAGETAG` (`ImageTagId`),
  CONSTRAINT `FK_Response_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Response_ExtraTextTag` FOREIGN KEY (`ExtraTextTagId`) REFERENCES `ExtraTextTag` (`TagId`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_Response_Function` FOREIGN KEY (`FunctionId`) REFERENCES `Function` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_Response_ImageTag` FOREIGN KEY (`ImageTagId`) REFERENCES `ImageTag` (`TagId`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ServerChannel`
--

DROP TABLE IF EXISTS `ServerChannel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServerChannel` (
  `Id` bigint(20) unsigned NOT NULL,
  `Name` text CHARACTER SET utf8mb4 NOT NULL,
  `Private` int(1) NOT NULL DEFAULT '0',
  `IsActive` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ServerChannelRestriction`
--

DROP TABLE IF EXISTS `ServerChannelRestriction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServerChannelRestriction` (
  `ServerChannelId` bigint(11) unsigned NOT NULL,
  `CommandId` int(11) NOT NULL,
  PRIMARY KEY (`ServerChannelId`,`CommandId`),
  KEY `FK_ServerChannelRestriction_Command` (`CommandId`),
  CONSTRAINT `FK_ServerChannelRestriction_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ServerChannelRestriction_ServerChannel` FOREIGN KEY (`ServerChannelId`) REFERENCES `ServerChannel` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ServerRole`
--

DROP TABLE IF EXISTS `ServerRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServerRole` (
  `Id` bigint(20) unsigned NOT NULL,
  `Name` text CHARACTER SET utf8mb4 NOT NULL,
  `Position` int(11) NOT NULL,
  `Color` int(10) unsigned NOT NULL,
  `IsActive` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ServerRoleRestriction`
--

DROP TABLE IF EXISTS `ServerRoleRestriction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServerRoleRestriction` (
  `ServerRoleId` bigint(11) unsigned NOT NULL,
  `CommandId` int(11) NOT NULL,
  PRIMARY KEY (`ServerRoleId`,`CommandId`),
  KEY `FK_ServerRoleRestriction_Command` (`CommandId`),
  CONSTRAINT `FK_ServerRoleRestriction_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ServerRoleRestriction_ServerRole` FOREIGN KEY (`ServerRoleId`) REFERENCES `ServerRole` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ServerUser`
--

DROP TABLE IF EXISTS `ServerUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServerUser` (
  `Id` bigint(20) unsigned NOT NULL,
  `CurrentUsername` text CHARACTER SET utf8mb4 NOT NULL,
  `CurrentNickname` text CHARACTER SET utf8mb4,
  `LastSpoke` datetime DEFAULT NULL,
  `JoinedAt` datetime NOT NULL,
  `LastOnlineAt` datetime DEFAULT NULL,
  `IsBot` int(1) NOT NULL DEFAULT '0',
  `IsBanned` int(1) NOT NULL DEFAULT '0',
  `IsOnServer` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ServerUserNameHistory`
--

DROP TABLE IF EXISTS `ServerUserNameHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServerUserNameHistory` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` bigint(20) unsigned NOT NULL,
  `Name` text COLLATE utf8mb4_unicode_ci,
  `Timestamp` datetime NOT NULL,
  `Nickname` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`Id`),
  KEY `FK_UserNameHistory_ServerUser` (`UserId`),
  CONSTRAINT `FK_UserNameHistory_ServerUser` FOREIGN KEY (`UserId`) REFERENCES `ServerUser` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ServerUserRestriction`
--

DROP TABLE IF EXISTS `ServerUserRestriction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServerUserRestriction` (
  `ServerUserId` bigint(11) unsigned NOT NULL,
  `CommandId` int(11) NOT NULL,
  PRIMARY KEY (`ServerUserId`,`CommandId`),
  KEY `FK_ServerUserRestriction_Command` (`CommandId`),
  CONSTRAINT `FK_ServerUserRestriction_Command` FOREIGN KEY (`CommandId`) REFERENCES `Command` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ServerUserRestriction_ServerUser` FOREIGN KEY (`ServerUserId`) REFERENCES `ServerUser` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ServerUserServerRole`
--

DROP TABLE IF EXISTS `ServerUserServerRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServerUserServerRole` (
  `ServerUserId` bigint(20) unsigned NOT NULL,
  `ServerRoleId` bigint(20) unsigned NOT NULL,
  KEY `FK_ServerUserServerRole_ServerRole` (`ServerRoleId`),
  KEY `FK_ServerUserServerRole_ServerUser` (`ServerUserId`),
  CONSTRAINT `FK_ServerUserServerRole_ServerRole` FOREIGN KEY (`ServerRoleId`) REFERENCES `ServerRole` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ServerUserServerRole_ServerUser` FOREIGN KEY (`ServerUserId`) REFERENCES `ServerUser` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Tag`
--

DROP TABLE IF EXISTS `Tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tag` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Tag` text CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TriviaCategory`
--

DROP TABLE IF EXISTS `TriviaCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TriviaCategory` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TriviaQuestion`
--

DROP TABLE IF EXISTS `TriviaQuestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TriviaQuestion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Question` text CHARACTER SET latin1 NOT NULL,
  `Answer` text CHARACTER SET latin1 NOT NULL,
  `Category` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_TriviaQuestion_TriviaCategory` (`Category`),
  CONSTRAINT `FK_TriviaQuestion_TriviaCategory` FOREIGN KEY (`Category`) REFERENCES `TriviaCategory` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=45673 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TriviaScore`
--

DROP TABLE IF EXISTS `TriviaScore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TriviaScore` (
  `ServerUserId` bigint(20) unsigned NOT NULL,
  `TotalScore` bigint(20) unsigned NOT NULL,
  `RoundScore` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`ServerUserId`),
  CONSTRAINT `FK_TriviaScore_ServerUser` FOREIGN KEY (`ServerUserId`) REFERENCES `ServerUser` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-11 19:46:35
