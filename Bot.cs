﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LGCBot.DataModels;
using LinqToDB.DataProvider.MySql;
using System.IO;
using LinqToDB;

namespace LGCBot {
    partial class Bot {
        DiscordClient client;
        static string connectionString;
        static readonly Random random = new Random();
        static string catApiKey;

        static void Main(string[] args) => new Bot().Start(args);

        private static ulong[] parseIdList(string ids) {
            try {
                return ids.Split(',').Select(s => ulong.Parse(s)).ToArray();
            } catch {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("Unable to parse id list: {0}.", ids);
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;

                return new ulong[0];
            }
        }
        
        public void Start(string[] args) {
            //LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
            
            var config = Merthsoft.DynamicConfig.Config.ReadIni("lgcbot.ini", new Merthsoft.DynamicConfig.ConfigOptions() { CaseSensitive = false });
            string protocol = "socket";
            try {
                protocol = config.Database.protocol;
            } catch {
                //WriteErrorToConsole("Protocol not defined, defaulting to socket.");
            }

            catApiKey = config.Cat.key;

            Console.WriteLine("Parsing trivia hint delay.");
            hint_delay = int.Parse(config.Trivia.hintDelay);

            Console.WriteLine("Parsing connection string.");
            connectionString = string.Format("Server={0};Database={2};Uid={3};Pwd={1};Protocol={4}", config.Database.server, config.Database.password, config.Database.database, config.Database.user, protocol);
            Console.WriteLine(connectionString);
            lgcbotDB.AddConfiguration("def", connectionString, new MySqlDataProvider());
            lgcbotDB.DefaultConfiguration = "def";

            client = new DiscordClient(x => {
                x.AppName = config.bot.appname;
                x.AppUrl = config.bot.appurl;
                x.MessageCacheSize = 0;
                x.UsePermissionsCache = true;
                x.EnablePreUpdateEvents = true;
                x.LogLevel = LogSeverity.Debug;
                x.LogHandler = OnLogMessage;
            });

            client.MessageReceived += Client_MessageReceived;
            client.ServerAvailable += Client_ServerAvailable;
            client.UserUpdated += Client_UserUpdated;
            client.UserJoined += Client_UserJoined;
            client.UserBanned += Client_UserBanned;
            client.UserLeft += Client_UserLeft;
            client.UserUnbanned += Client_UserUnbanned;

            client.ExecuteAndWait(async () => {
                while (true) {
                    try {
                        await client.Connect(config.bot.token);
                        client.SetGame(config.bot.game);

                        break;
                    } catch (Exception ex) {
                        client.Log.Error($"Login Failed", ex);
                        await Task.Delay(client.Config.FailedReconnectDelay);
                    }
                }
            });
        }

        private void Client_UserUnbanned(object sender, UserEventArgs e) {
            using (var db = new lgcbotDB("def")) {
                var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == e.User.Id);
                dbUser.IsBanned = 0;
                db.BeginTransaction();
                updateUser(db, e.User);
                updateUserRole(db, e.User);
                db.CommitTransaction();
            }
        }

        private void Client_UserLeft(object sender, UserEventArgs e) {
            using (var db = new lgcbotDB("def")) {
                var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == e.User.Id);
                dbUser.IsOnServer = 0;
                db.BeginTransaction();
                updateUser(db, e.User);
                updateUserRole(db, e.User);
                db.CommitTransaction();
            }
        }

        private void Client_UserBanned(object sender, UserEventArgs e) {
            using (var db = new lgcbotDB("def")) {
                var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == e.User.Id);
                dbUser.IsBanned = 1;
                dbUser.IsOnServer = 0;
                db.BeginTransaction();
                updateUser(db, e.User);
                updateUserRole(db, e.User);
                db.CommitTransaction();
            }
        }

        private void Client_UserJoined(object sender, UserEventArgs e) {
            using (var db = new lgcbotDB("def")) {
                db.BeginTransaction();
                updateUser(db, e.User);
                updateUserRole(db, e.User);
                db.CommitTransaction();
            }
        }

        private void Client_UserUpdated(object sender, UserUpdatedEventArgs e) {
            using (var db = new lgcbotDB("def")) {
                db.BeginTransaction();
                updateUser(db, e.After);
                updateUserRole(db, e.After);
                db.CommitTransaction();
            }
        }

        private static void updateUser(lgcbotDB db, User u) {
            var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == u.Id);
            var updatedUser = new ServerUser() {
                Id = u.Id, CurrentUsername = u.Name, CurrentNickname = u.Nickname, IsBot = u.IsBot ? 1 : 0, IsBanned = 0,
                IsOnServer = 1, JoinedAt = u.JoinedAt, LastOnlineAt = u.LastOnlineAt ?? DateTime.UtcNow, LastSpoke = u.LastActivityAt ?? dbUser?. LastSpoke
            };

            if (dbUser == null) {
                db.Insert(updatedUser);
            } else {
                if (u.Name != dbUser.CurrentUsername || u.Nickname != dbUser.CurrentNickname) {
                    var history = new ServerUserNameHistory() {
                        UserId = dbUser.Id,
                        Name = (u.Name != dbUser.CurrentUsername ? dbUser.CurrentUsername : null),
                        Nickname = (u.Nickname != dbUser.CurrentNickname ? dbUser.CurrentNickname : null),
                        Timestamp = DateTime.UtcNow
                    };
                    db.InsertWithIdentity(history);
                }
                db.Update(updatedUser);
            }
        }

        private static void updateUserRole(lgcbotDB db, User u) {
            db.ServerUserServerRoles.Delete(r => r.ServerUserId == u.Id);
            foreach (var role in u.Roles) {
                db.Insert(new ServerUserServerRole() { ServerUserId = u.Id, ServerRoleId = role.Id });
            }
        }

        private void Client_ServerAvailable(object sender, ServerEventArgs e) {
            var server = e.Server;

            logInfo("Client_JoinedServer", "Processing roles.");
            using (var db = new lgcbotDB("def")) {
                db.BeginTransaction();
                db.ServerRoles.Set(r => r.IsActive, 0).Update();

                foreach (var r in server.Roles) {
                    var updatedRole = new ServerRole() { Id = r.Id, Color = r.Color.RawValue, IsActive = 1, Name = r.Name, Position = r.Position };
                    var dbRole = db.ServerRoles.FirstOrDefault(sr => sr.Id == r.Id);

                    if (dbRole == null) {
                        db.Insert(updatedRole);
                    } else {
                        db.Update(updatedRole);
                    }
                }

                db.CommitTransaction();
            }

            logInfo("Client_JoinedServer", "Processing channels.");
            using (var db = new lgcbotDB("def")) {
                db.BeginTransaction();
                db.ServerChannels.Set(r => r.IsActive, 0).Update();

                foreach (var c in server.AllChannels) {
                    var updatedChannel = new ServerChannel() {  Id = c.Id, IsActive = 1, Name = c.Name, Private = c.IsPrivate ? 1 : 0 };
                    var dbChannel = db.ServerChannels.FirstOrDefault(sc => sc.Id == c.Id);

                    if (dbChannel == null) {
                        db.Insert(updatedChannel);
                    } else {
                        db.Update(updatedChannel);
                    }
                }

                db.CommitTransaction();
            }

            logInfo("Client_JoinedServer", "Processing users.");
            using (var db = new lgcbotDB("def")) {
                db.BeginTransaction();
                db.ServerUsers.Set(u => u.IsOnServer, 0);

                foreach (var u in server.Users) {
                    updateUser(db, u);
                }

                db.CommitTransaction();
            }

            logInfo("Client_JoinedServer", "Processing user roles.");
            using (var db = new lgcbotDB("def")) {
                db.BeginTransaction();

                foreach (var u in server.Users) {
                    updateUserRole(db, u);
                }

                db.CommitTransaction();
            }
        }

        private static void SendMessage(Channel channel, string format, params object[] args) {
            string message = string.Format(format, args);
            channel.SendMessage(message);
        }

        private void Client_MessageReceived(object sender, MessageEventArgs e) {
            using (var db = new lgcbotDB("def")) {
                var dbUser = db.ServerUsers.FirstOrDefault(su => su.Id == e.User.Id);
                dbUser.LastSpoke = DateTime.UtcNow;
                db.Update(dbUser);
            }

            if (e.User.IsBot) { return; }

            var msg = e.Message.Text.TrimEnd(' ').Split(' ');
            var command = msg[0].ToUpper();
            var args = msg.Skip(1).ToArray();

            try {
                readDbCommand(e, command, args);
            } catch (Exception ex) {
                OnLogMessage(this, new LogMessageEventArgs(LogSeverity.Error, "Client_MessageReceived", string.Format("Client_MessageReceived", "Error processing {0}: {1}", command, ex.Message), ex));
            }
        }

        private void logInfo(string source, string format, params object[] args) {
            OnLogMessage(this, new LogMessageEventArgs(LogSeverity.Info, source, string.Format(format, args), null));
        }

        private bool readDbCommand(MessageEventArgs eventArgs, string command, string[] args) {
            using (var db = new lgcbotDB("def")) {
                logInfo("readDbCommand", "Created db. Is null: {0}", db == null);

                try {
                    // Get exact-number-of-parameters command
                    var response = (from r in db.Responses.LoadWith(r => r.Function)
                                    join ct in db.CommandTexts
                                    on r.CommandId equals ct.CommandId
                                    where ct.Text.ToUpper() == command.ToUpper()
                                    where r.NumParameters == args.Length
                                    select r).FirstOrDefault();
                    logInfo("readDbCommand", "First response object: {0}", response != null);
                    // Get command with any-number-of-parameters if that fails
                    if (response == null) {
                        response = (from ct in db.CommandTexts
                                    join r in db.Responses.LoadWith(r => r.Function)
                                         on ct.CommandId equals r.CommandId
                                    where ct.Text.ToUpper() == command.ToUpper()
                                    where r.NumParameters < 0
                                    select r).FirstOrDefault();
                        logInfo("readDbCommand", "Second response object: {0}", response != null);
                    }

                    // And, finally, check if there's an "all text" command for this channel
                    if (response == null) {
                        response = (from cr in db.ServerChannelRestrictions
                                    join r in db.Responses.LoadWith(r => r.Function)
                                         on cr.CommandId equals r.CommandId
                                    where cr.CommandId == -1
                                    where cr.ServerChannelId == eventArgs.Channel.Id
                                    select r).FirstOrDefault();
                        logInfo("readDbCommand", "Third response object: {0}", response != null);
                        if (response == null) {
                            return false;
                        }
                    }
                    OnLogMessage(this, new LogMessageEventArgs(LogSeverity.Verbose, "readDbCommand", "SQL text: " + db?.LastQuery, null));

                    response.Command = db.Commands.FirstOrDefault(c => c.Id == response.CommandId);
                    logInfo("readDbCommand", "Command loaded: {0}", response.Command != null);
                    
                    if (response.NumParameters == -2 && args.Length == 0) {
                        logInfo("readDbCommand", "Parameter mismatch.");
                        SendMessage(eventArgs.Channel, response.FailedParameterCheckText, eventArgs.User.NicknameMention, "", "");
                        return false;
                    }

                    logInfo("readDbCommand", "Checking restrictions.");
                    if (!checkResponseRestrictions(db, eventArgs, response)) {
                        logInfo("readDbCommand", "Restriction mismatch.");
                        return false;
                    }

                    logInfo("readDbCommand", "Outputting.");
                    outputText(eventArgs, args, db, response);
                    displayImage(eventArgs, db, response);
                    handleFunction(eventArgs, db, command, args, response);

                    return true;
                } catch (Exception ex) {
                    OnLogMessage(this, new LogMessageEventArgs(LogSeverity.Verbose, "readDbCommand", "Error occured. SQL text: " + db?.LastQuery, ex));
                    throw;
                }
            }
        }

        private bool checkResponseRestrictions(lgcbotDB db, MessageEventArgs eventArgs, Response response) {
            logInfo("checkResponseRestrictions", "Checking channel resitrctions.");
            var restrictedChannels = db.ServerChannelRestrictions.Where(c => c.CommandId == response.CommandId).Select(c => c.ServerChannelId);
            if (restrictedChannels.Count() > 0) {
                var currentChannel = eventArgs.Channel;
                if (!restrictedChannels.Contains(currentChannel.IsPrivate ? 0 : currentChannel.Id)) {
                    SendMessage(eventArgs.Channel, response.FailedChannelCheckText, eventArgs.User.NicknameMention, "", "");
                    return false;
                }
            }

            logInfo("checkResponseRestrictions", "Checking roles resitrctions.");
            var restrictedRoles = db.ServerRoleRestrictions.Where(r => r.CommandId == response.CommandId).Select(r => r.ServerRoleId);
            if (restrictedRoles.Count() > 0) {
                var userRoles = eventArgs.User.Roles.Select(r => r.Id);
                if (userRoles.Count(r => restrictedRoles.Contains(r)) == 0) {
                    SendMessage(eventArgs.Channel, response.FailedRoleCheckText, eventArgs.User.NicknameMention, "", "");
                    return false;
                }
            }

            logInfo("checkResponseRestrictions", "Checking user resitrctions.");
            var restrictedUsers = db.ServerUserRestrictions.Where(u => u.CommandId == response.CommandId).Select(u => u.ServerUserId);
            if (restrictedUsers.Count() != 0) {
                if (!restrictedUsers.Contains(eventArgs.User.Id)) {
                    SendMessage(eventArgs.Channel, response.FailedUserCheckText, eventArgs.User.NicknameMention, "", "");
                    return false;
                }
            }

            return true;
        }

        private static void outputText(MessageEventArgs eventArgs, string[] args, lgcbotDB db, Response response) {
            if (response.Text != null) {
                ExtraText extraText = null;
                // Get a random "extra text" based on tag
                if (response.ExtraTextTagId.HasValue) {
                    var query = db.ExtraTextTags.LoadWith(ett => ett.ExtraText).Where(ett => ett.TagId == response.ExtraTextTagId);
                    if (query.Count(ett => ett.ExtraText.Used == 0) == 0) {
                        db.ExtraTexts.Where(et => query.Select(ett => ett.ExtraTextId).Contains(et.Id)).Set(et => et.Used, 0).Update();
                    } else {
                        query = query.Where(ett => ett.ExtraText.Used == 0);
                    }
                    extraText = query.ElementAt(random.Next(query.Count())).ExtraText;
                    db.ExtraTexts.Where(et => et.Id == extraText.Id).Set(et => et.Used, 1).Update();
                }

                SendMessage(eventArgs.Channel, response.Text, eventArgs.User.NicknameMention, string.Join(" ", args), extraText?.Text);
            }
        }

        private static void displayImage(MessageEventArgs eventArgs, lgcbotDB db, Response response) {
            if (response.ImageTagId.HasValue) {
                // Get a random image based on tag
                var query = db.ImageTags.LoadWith(it => it.Image).Where(it => it.TagId == response.ImageTagId);
                if (query.Count(it => it.Image.Used == 0) == 0) {
                    db.Images.Where(it => query.Select(i => i.ImageId).Contains(it.Id)).Set(i => i.Used, 0).Update();
                } else {
                    query = query.Where(it => it.Image.Used == 0);
                }
                var image = query.ElementAt(random.Next(query.Count())).Image;

                db.Images.Where(i => i.Id == image.Id).Set(i => i.Used, 1).Update();

                using (MemoryStream ms = new MemoryStream(image.Image_Column)) {
                    eventArgs.Channel.SendFile("image." + image.Extension, ms);
                }
            }
        }
        
        private void handleFunction(MessageEventArgs eventArgs, lgcbotDB db, string command, string[] args, Response response) {
            if (response.FunctionId != null) {
                logInfo("handleFunction", "Loading function with id: {0}", response.FunctionId);

                response.Function = db.Functions.FirstOrDefault(f => f.Id == response.FunctionId);

                if (response.Function == null) { return; }

                logInfo("handleFunction", "Checking for function: {0}", response.Function.Name);
                var thisType = GetType();
                var func = thisType.GetMethod(response.Function.Name);
                logInfo("handleFunction", "Function found: {0}", func != null);
                func?.Invoke(this, new object[] { eventArgs, command, args });

                if (func == null) {
                    OnLogMessage(this,
                        new LogMessageEventArgs(LogSeverity.Error, "readDbCommand " + command, "Function not found: " + response.Function.Name, null)
                    );
                    SendMessage(eventArgs.Channel, "{0}: Command {1} failed.", eventArgs.User, command);
                }
            }
        }
        
        private void OnLogMessage(object sender, LogMessageEventArgs e) {
            //Color
            ConsoleColor color;
            switch (e.Severity) {
                case LogSeverity.Error: color = ConsoleColor.Red; break;
                case LogSeverity.Warning: color = ConsoleColor.Yellow; break;
                case LogSeverity.Info: color = ConsoleColor.White; break;
                case LogSeverity.Verbose: color = ConsoleColor.Gray; break;
                case LogSeverity.Debug: default: color = ConsoleColor.DarkGray; break;
            }

            //Exception
            string exMessage;
            Exception ex = e.Exception;
            if (ex != null) {
                while (ex is AggregateException && ex.InnerException != null)
                    ex = ex.InnerException;
                exMessage = ex.Message;
            } else {
                exMessage = null;
            }

            //Source
            string sourceName = e.Source?.ToString();

            //Text
            string text;
            if (e.Message == null) {
                text = exMessage ?? "";
                exMessage = null;
            } else {
                text = e.Message;
            }

            //Build message
            StringBuilder builder = new StringBuilder(text.Length + (sourceName?.Length ?? 0) + (exMessage?.Length ?? 0) + 5);
            if (sourceName != null) {
                builder.Append('[');
                builder.Append(sourceName);
                builder.Append("] ");
            }

            for (int i = 0; i < text.Length; i++) {
                //Strip control chars
                char c = text[i];
                if (!char.IsControl(c)) {
                    builder.Append(c);
                }
            }

            if (exMessage != null) {
                builder.Append(": ");
                builder.Append(exMessage);
            }

            text = builder.ToString();
            Console.ForegroundColor = color;
            Console.WriteLine(text);
        }
    }
}
