using Discord;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGCBot {
    public class Command {
        public Action<MessageEventArgs, string, string[]> Action { get; set; }
        public CommandChannelType ChannelType { get; set; }
        public UserType UserType { get; set; }

        public Command(Action<MessageEventArgs, string, string[]> action) {
            this.Action = action;
            ChannelType = CommandChannelType.Standard;
            UserType = UserType.Standard;
        }
    }
}
