﻿using Discord;
using LGCBot.DataModels;
using LinqToDB;
using LinqToDB.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LGCBot {
    partial class Bot {
        static int hint_delay;
        static bool inTriviaGame = false;
        static TriviaQuestion currentQuestion;
        static CancellationTokenSource questionAnsweredToken;
        static int noAnswerCount = 0;
        static List<int> categories = null;

        public static void ListCategories(MessageEventArgs arg1, string arg2, string[] arg3) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("The following categories are available:");

            using (var db = new lgcbotDB("def")) {
                foreach (var cat in from c in db.TriviaCategories select new { Name = c.Name, NumQuestions = c.TriviaQuestions.Count() }) {
                    sb.AppendLine(string.Format("\t{0} ({1} questions)", cat.Name, cat.NumQuestions));
                }
            }

            SendMessage(arg1.Channel, sb.ToString());
        }

        public static void TriviaHelp(MessageEventArgs e, string command, IList<string> parameters) {
            StringBuilder sb = new StringBuilder();

            if (parameters.Count == 0 || parameters[0].ToLowerInvariant() != "more") {
                sb.AppendLine("This is Let's Grab Coffe Trivia! To play, just type *?trivia*, and then type in your answers. To end type *?stop*. Type *?help more* for more.");
            } else {
                sb.AppendLine("Available commands:");
                sb.AppendLine("\t*?help* - Displays basic help");
                sb.AppendLine("\t*?help more* - Displays this list of commands");
                sb.AppendLine("\t*?trivia* - Starts trivia");
                sb.AppendLine("\t*?start* - Starts trivia");
                sb.AppendLine("\t*?trivia category 1,category 2, ...* - Starts trivia using only the given categories");
                sb.AppendLine("\t*?start category 1,category 2, ...* - Starts trivia using only the given categories");
                sb.AppendLine("\t*?stop* - Stops trivia");
                sb.AppendLine("\t*?end* - Stops trivia");
                sb.AppendLine("\t*?categories* - Shows a list of the available categories");
                sb.AppendLine("\t*?score* - Gives your score");
                sb.AppendLine("\t*?scores* - During a round of trivia, displays the current round scores; otherwise, shows the top three scores");
                sb.AppendLine("\t*?top* - Shows the top three scores");
            }

            SendMessage(e.Channel, sb.ToString());
        }

        static string normalize(string text) {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString) {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark) {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC).ToLowerInvariant();
        }

        public static void HandleTrivia(MessageEventArgs e, string command, string[] args) {
            noAnswerCount = 0;

            if (!inTriviaGame || currentQuestion == null) { return; }
            var answer = normalize(currentQuestion.Answer);
            var guess = normalize(e.Message.Text);

            if (answer == guess) {
                questionAnsweredToken.Cancel();
                SendMessage(e.Channel, "{0} got the right answer! {1}", e.User.NicknameMention, currentQuestion.Answer);
                using (var db = new lgcbotDB("def")) {
                    bool insert = false;
                    var currentScore = db.TriviaScores.Where(u => u.ServerUserId == e.User.Id).FirstOrDefault();
                    if (currentScore == null) {
                        currentScore = new TriviaScore() {
                            RoundScore = 0,
                            TotalScore = 0,
                            ServerUserId = e.User.Id
                        };
                        insert = true;
                    }
                    
                    currentScore.RoundScore += 1;
                    currentScore.TotalScore += 1;

                    if (insert) {
                        db.Insert(currentScore);
                    } else {
                        db.TriviaScores.Update(c => c.ServerUserId == currentScore.ServerUserId, c => new TriviaScore() {
                            ServerUserId = currentScore.ServerUserId,
                            RoundScore = currentScore.RoundScore,
                            TotalScore = currentScore.TotalScore
                        });
                    }
                }

                currentScores(e);
            }
        }

        public static void UserScore(MessageEventArgs e, string command, IList<string> parameters) {
            using (var db = new lgcbotDB("def")) {
                var score = db.TriviaScores.Where(u => u.ServerUserId == e.User.Id).FirstOrDefault();
                if (score == null) {
                    score = new TriviaScore() {
                        RoundScore = 0,
                        TotalScore = 0,
                        ServerUserId = e.User.Id
                    };
                    db.Insert(score);
                }

                string message = "{0}: You have of an overall score of {1}";
                if (inTriviaGame) {
                    message += " and a current score of {2}";
                }
                message += ".";

                SendMessage(e.Channel, message, e.User.NicknameMention, score.TotalScore, score.RoundScore);
            }
        }

        public static void TopScores(MessageEventArgs eventArgs, string command, IList<string> parameters) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Top three scores:");
            using (var db = new lgcbotDB("def")) {
                var query = from s in db.TriviaScores.LoadWith(s => s.ServerUser)
                            orderby s.TotalScore descending
                            where s.TotalScore > 0
                            select new { Score = s.TotalScore, Name = s.ServerUser.CurrentNickname ?? s.ServerUser.CurrentUsername };
                foreach (var score in query.Take(3)) {
                    sb.AppendLine(string.Format("{0}: {1}", score.Name, score.Score));
                }
            }
            SendMessage(eventArgs.Channel, sb.ToString());
        }

        private static void currentScores(MessageEventArgs eventArgs) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Current scores:");
            using (var db = new lgcbotDB("def")) {
                StringBuilder sb2 = new StringBuilder();
                foreach (var score in from s in db.TriviaScores.LoadWith(s => s.ServerUser)
                                      orderby s.RoundScore descending
                                      where s.RoundScore > 0
                                      select new { Score = s.RoundScore, Name = s.ServerUser.CurrentNickname ?? s.ServerUser.CurrentUsername }) {
                    sb2.AppendLine(string.Format("{0}: {1}", score.Name, score.Score));
                }
                if (sb2.Length > 0) {
                    sb.Append(sb2);
                    SendMessage(eventArgs.Channel, sb.ToString());
                }
            }            
        }

        public static void CurrentScores(MessageEventArgs eventArgs, string command, IList<string> parameters) {
            if (!inTriviaGame) {
                TopScores(eventArgs, command, parameters);
                return;
            }

            currentScores(eventArgs);
        }

        public static void StopGame(MessageEventArgs eventArgs, string command, IList<string> parameters) {
            inTriviaGame = false;
            questionAnsweredToken.Cancel();
        }

        public static void StartGame(MessageEventArgs eventArgs, string command, IList<string> parameters) {
            if (inTriviaGame) {
                SendMessage(eventArgs.Channel, "{0}: Game already in progress. Current question: {1}", eventArgs.User.NicknameMention, currentQuestion?.Question);
                return;
            }

            inTriviaGame = true;
            Task.Run(() => game(eventArgs, parameters));
        }

        private static string markdownEscape(string input) {
            return input.Replace("\\", "\\\\").Replace("*", "\\*").Replace("_", "\\_").Replace("~", "\\~");
        }

        private static async void game(MessageEventArgs eventArgs, IList<string> parameters) {
            Channel channel = eventArgs.Channel;

            if (parameters.Count() == 0) {
                categories = null;
            } else {
                categories = new List<int>();
                foreach (var cat in string.Join(" ", parameters).Split(',')) {
                    using (var db = new lgcbotDB("def")) {
                        var c = db.TriviaCategories.FirstOrDefault(a => a.Name.ToLower() == cat.Trim().ToLower());
                        if (c == null) {
                            SendMessage(channel, "Category {0} not found; not starting trivia.", cat);
                            inTriviaGame = false;
                            return;
                        }
                        categories.Add(c.Id);
                    }
                }
            }

            SendMessage(channel, "Welcome to trivia!");
            //SendMessage(channel, "@everyone please get ready to play!");

            using (var db = new lgcbotDB("def")) {
                db.TriviaScores.Set(s => s.RoundScore, (ulong)0).Update();
            }

            int questionNumber = 0;
            questionAnsweredToken = new CancellationTokenSource();

            while (inTriviaGame) {
                await Task.Delay(250);
                if (!inTriviaGame) { break; }

                using (var db = new lgcbotDB("def")) {
                    var set = db.TriviaQuestions
                        .Select(q => new TriviaQuestion() { Answer = q.Answer, Question = q.Question, Category = q.Category })
                        .Where(q => categories == null ? true : categories.Contains(q.Category));
                    currentQuestion = set.ElementAt(random.Next(set.Count()));
                    currentQuestion.Answer = markdownEscape(currentQuestion.Answer);
                    currentQuestion.Question = markdownEscape(currentQuestion.Question);
                }

                char[] hint = currentQuestion.Answer.Select(c => char.IsLetterOrDigit(c) ? '-' : c).ToArray();
                int answerLength = currentQuestion.Answer.Length;

                questionNumber++;
                SendMessage(channel, "Question {0}: {1}", questionNumber, currentQuestion.Question);
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine(string.Format("Question answer: {0}", currentQuestion.Answer));
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;

                questionAnsweredToken = new CancellationTokenSource();
                try { await Task.Delay(hint_delay, questionAnsweredToken.Token); } catch { }
                if (questionAnsweredToken.IsCancellationRequested) { continue; }

                doHint(channel, currentQuestion.Answer, ref hint, 0);

                try { await Task.Delay(hint_delay, questionAnsweredToken.Token); } catch { }
                if (questionAnsweredToken.IsCancellationRequested) { continue; }

                if (answerLength > 1) {
                    doHint(channel, currentQuestion.Answer, ref hint, 1);
                }

                try { await Task.Delay(hint_delay, questionAnsweredToken.Token); } catch { }
                if (questionAnsweredToken.IsCancellationRequested) { continue; }

                
                doHint(channel, currentQuestion.Answer, ref hint, 3);
                
                try { await Task.Delay(hint_delay, questionAnsweredToken.Token); } catch { }
                if (questionAnsweredToken.IsCancellationRequested) { continue; }
                
                doHint(channel, currentQuestion.Answer, ref hint, 6);

                try { await Task.Delay(hint_delay, questionAnsweredToken.Token); } catch { }
                if (questionAnsweredToken.IsCancellationRequested) { continue; }

                SendMessage(channel, "TIME'S UP! The answer is: {0}", currentQuestion.Answer);
                currentQuestion = null;
                noAnswerCount++;

                if (noAnswerCount == 3) {
                    SendMessage(channel, "Too many questions have been asked without any attempted answers. Stopping trivia. Use *?trivia* to start again!");
                    inTriviaGame = false;
                }
            }
            
            currentScores(eventArgs);
            SendMessage(channel, "Done playing trivia. Congrats to all!");
        }

        private static void doHint(Channel channel, string answer, ref char[] hint, int numChars) {
            while (hint.Count(c => c == '-') <= numChars) { numChars--; }
            if (numChars < 0) { return; }

            for (int i = 0; i < numChars; i++) {
                int randomIndex;
                do {
                    randomIndex = random.Next(hint.Length);
                } while (hint[randomIndex] != '-');
                hint[randomIndex] = answer[randomIndex];
            }

            SendMessage(channel, "Hint: {0}", new string(hint));
        }
    }
}
